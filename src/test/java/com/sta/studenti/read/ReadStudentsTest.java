package com.sta.studenti.read;

import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class ReadStudentsTest {

    private static final String path = "D:\\Andrei\\Java\\FisiereIO\\students.txt";

    @Test
    public void readStudentsAsListTest () {

        ReadStudents reader = new ReadStudents();

        //instantiate an arraylist as expected list
        List<Student> expectedList = new ArrayList<>();

        //instantiate students to populate the list
        Student s1 = new Student("Monica", "Lupu", 20, "Chimie");
        Student s2 = new Student("Andrei", "Sandu", 19, "Biologie");
        Student s3 = new Student("Bubu", "Manalachioaie", 24, "Arte");
        Student s4 = new Student("Georgel", "Timoiu", 18, "Informatica");
        Student s5 = new Student("Paul", "Ghiunea", 21, "Chimie");

        //add created students to list
        expectedList.add(s1);
        expectedList.add(s2);
        expectedList.add(s3);
        expectedList.add(s4);
        expectedList.add(s5);

        //call the method to read the students from file and returned is as a list
        List<Student> actualList = reader.readStudentsAsList(path);

        Assert.assertEquals(expectedList, actualList);
    }
}
