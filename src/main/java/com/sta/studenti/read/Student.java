package com.sta.studenti.read;

public class Student {

    private String firstName;
    private String lastName;
    private int age;
    private String college;

    public Student(String firstName, String lastName, int age, String college) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.age = age;
        this.college = college;
    }

    public Student() {
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getCollege() {
        return college;
    }

    public void setCollege(String college) {
        this.college = college;
    }

    //need to override toString() to display a student instance as dev wish, else a memory area will me displayed
    @Override
    public String toString() {
        return firstName + " " + lastName + " " + age + " years ->" + college;
    }

    //need to override equals() because it is used in unit testing in assert() method
    //if you didn't explain to jvm what makes two students equal, then equals() will return true only if two student references
    //refer to the same instance of student
    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }

        if (obj instanceof Student) {
            Student objStudent = (Student) obj;
            if(objStudent.firstName.equals(this.firstName) && objStudent.lastName.equals(this.lastName) &&
                    (objStudent.age == this.age) && objStudent.college.equals(this.college)) {
                return true;
            }
        }

        return false;
    }
}
