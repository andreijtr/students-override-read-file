package com.sta.studenti.read;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class ReadStudents {

    public List<Student> readStudentsAsList (String path) {

        //create a list that will be returned
        List<Student> listOfStudents = new ArrayList<>();

        File file = new File(path);

        try {
            FileReader reader = new FileReader(file);
            BufferedReader buffered = new BufferedReader(reader);

            String line = buffered.readLine();

            while (line != null) {
                String[] splitLine = line.split(" ");

                //read line, create a String[] array, instantiate a student using elements of array as parameters
                Student s = new Student();
                s.setFirstName(splitLine[0]);
                s.setLastName(splitLine[1]);
                s.setAge(Integer.valueOf(splitLine[2]));
                s.setCollege(splitLine[3]);

                //add readed student to list
                listOfStudents.add(s);

                //read next line
                line = buffered.readLine();
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException ex) {
            ex.printStackTrace();
        }

        return listOfStudents;
    }
}
