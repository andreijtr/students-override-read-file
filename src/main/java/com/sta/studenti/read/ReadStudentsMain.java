package com.sta.studenti.read;

import java.util.Iterator;
import java.util.List;

public class ReadStudentsMain {

    private static final String path = "D:\\Andrei\\Java\\FisiereIO\\students.txt";

    public static void main(String[] args) {

        ReadStudents reader = new ReadStudents();

        List<Student> returnedList = reader.readStudentsAsList(path);

        Iterator<Student> iterator = returnedList.iterator();

        while (iterator.hasNext()) {
            System.out.println(iterator.next());
        }
    }
}
